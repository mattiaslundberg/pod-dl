Download podcast episodes

# Usage

1. Clone git repo
2. Install dependencies: `npm install`
3. Run `node download.js -u <FEED URL> -c <COUNT> -l <LOCATION>`

`<FEED URL>` is the RSS url for the podcast feed, `<COUNT>` is the number of episodes to download (default 10) and `<LOCATION>` is the target directory (default current directory).
