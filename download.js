const { ArgumentParser } = require("argparse");
let Parser = require("rss-parser");
let parser = new Parser();

const http = require("https");
const fs = require("fs");

const fileExists = (dest) => {
  return new Promise((resolve) => {
    fs.stat(dest, (err) => {
      resolve(!err);
    });
  });
};

const doGet = (url, file, cb) => {
  return new Promise((resolve) => {
    http.get(url, async (response) => {
      if ([301, 302].includes(response.statusCode)) {
        await doGet(response.headers.location, file, cb);
        resolve();
      }
      response.pipe(file);
      file.on("finish", () => {
        file.close(() => {
          resolve();
        });
      });
    });
  });
};

const download = async (url, dest) => {
  if (await fileExists(dest)) {
    console.log("Not downloading since file exists");
    return;
  }

  console.log(`Downloading: ${dest}`);
  return new Promise(async (resolve) => {
    let file = fs.createWriteStream(dest);
    await doGet(url, file);
    resolve();
  });
};

const main = async () => {
  const args = new ArgumentParser({ description: "Podcast downloader" });
  args.add_argument("-u", "--url", { help: "Feed URL", required: true });
  args.add_argument("-c", "--count", {
    help: "Number of episodes",
    default: 10,
  });
  args.add_argument("-l", "--location", {
    help: "Download to location",
    default: ".",
  });

  const { url, count, location } = args.parse_args();

  const feed = await parser.parseURL(url);

  feed.items.splice(0, count).forEach(async ({ title, enclosure }) => {
    const url = enclosure.url;
    await download(url, `${location}/${feed.title} - ${title}.mp3`);
  });
};

main();
